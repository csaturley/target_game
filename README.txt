Target Game
Created by: Sarah Raptis, China Saturley, and Emily Meave
Background created by Sarah Raptis
Shooting sound effect created by Mike Koenig and obtained from soundbible.com/royalty-free-sounds-11.html.

Overview: This program is a simple, circus-themed target shooting gallery game. Playing this game involves users going after a moving target with their mouse and attempting to click on it.
Instructions: Press the "ENTER" or "RETURN" key to start the game. Once the game starts, move your mouse and click on the target as many times as you can. Once the game is over, press "ENTER" or "RETURN" to play again.
