class Target {
  

  int size, location_x, location_y, red, white;
  
  Target() {
  
    size = int(random(10,50));
    location_x = int(random(30,400));
    location_y = int(random(30,400));
    red = 255;
    white = 255;
  
  }
  
  void display() {
    
    fill(red, white);
    ellipse(location_x, location_y, size, size);
  
  }
  
  void setColor(int r, int w) {
    
    red = r;
    white = w;
    
  }
  
  void setSize(int s) {
    
    size = s;
    
  }
  
  int getSize() {
    
    return size;
    
  }
  
  int[] getcolor() {
    
    int[] rw = {red, white};
    return rw;
    
  }
  
  void drawTarget(float tx, float ty, int tw, int th) {
    location_x = int(tx);
    location_y = int(ty);
    fill(255,0,0);
    ellipse(tx, ty, tw, th);
    fill(255,255,255);
    ellipse(tx,ty,tw-40,th-40);
    fill(255,0,0);
    ellipse(tx,ty,tw-70,th-70);
    fill(255,255,255);
    ellipse(tx,ty,tw-100,th-100);
    fill(255,0,0);
    ellipse(tx,ty,tw-140,th-140);   
  }
}
