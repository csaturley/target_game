import java.util.Arrays;
import processing.sound.*;

//Setting up target1 and target2
Target target1 = new Target();
Target target2 = new Target();
//Setting up scene for gameplay
int Scene = 0;

//Setting up the canvas, image, and audio
PImage background;
SoundFile effect;
//Declaring coordinates of target
float x = 0;
float y = 0;

void setup() {
  size(726, 561);
  background = loadImage("background.PNG");
  background.resize(0,561);
  effect = new SoundFile(this, "fire_bow_sound-mike-koenig.wav");
  //Sound effect created by Mike Koenig and obtained 
  //from soundbible.com/royalty-free-sounds-11.html.
  effect.play();
}
//Draw
void draw() {
   background(background);
   if(Scene == 0){
     textSize(22);
     fill(0);
     text("Press ENTER/RETURN on the keyboard to start the target game.", 25, 280);
  } else if (Scene == 1){
    GamePlay();
  } else if (Scene == 2){
    gameover();
  }
}

//Keyboard input
void keyPressed(){
  if(Scene == 0 || Scene == 2){
    if(key == ENTER || key == RETURN){
      Scene = 1;
    }
  }
}

 //Other functions  

void GamePlay() {
  background(background);
  target1.drawTarget(x,y,200,200);
  x = x+10;
  y = y+6;
  
    if(mousePressed) {
      effect = new SoundFile(this, "fire_bow_sound-mike-koenig.wav");
  //Sound effect created by Mike Koenig and obtained
  //from soundbible.com/royalty-free-sounds-11.html.
  effect.play();
Scene ++;
}
}

void gameover() {
  background(0,0,50);
  text(" Game over great job you hit the target!", 25, 280);
text("By Sarah Raptis, Emily Meave, China Saturley", 45,400);
}
